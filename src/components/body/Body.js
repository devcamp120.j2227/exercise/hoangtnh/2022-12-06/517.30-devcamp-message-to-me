import React, { Component } from "react";
import InputMessage from "./body-input/InputMessage";
import LikeImage from "./body-output/LikeImage";

class Body extends Component {
    constructor(props){
        super(props);
        this.state = {
            inputMessage:"",
            outputMessage:[],
            likeImg: false
        }
    }
    //tạo 1 hàm cho phép thay đổi state inputMessage dựa vào chuỗi truyền vào
    inputMessageChangeHandler = (value)=>{
        this.setState({
            inputMessage: value
        })
    }
    //tạo 1 hàm cho phép thay đổi state outputMessage và likeImg
    outputMessageChangeHandler = () =>{
        if(this.state.inputMessage){
            this.setState({
                outputMessage: [...this.state.outputMessage ,this.state.inputMessage],
                likeImg: true
            })
        }
    }
    render() {
        return(
            <React.Fragment>
                <InputMessage inputMessageProp={this.state.inputMessage} inputMessageChangeHandlerProp={this.inputMessageChangeHandler} outputMessageChangeHandlerProp={this.outputMessageChangeHandler}/>
                <LikeImage outputMessageProp={this.state.outputMessage} likeImgProp={this.state.likeImg}/>
            </React.Fragment>
        )
    }
}

export default Body;