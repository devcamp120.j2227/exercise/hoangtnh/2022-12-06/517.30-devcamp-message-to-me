import { Component } from "react";

import likeImage from "../../../assets/images/like.png";

class LikeImage extends Component {
    render() {
        const {outputMessageProp, likeImgProp} = this.props;
        return(
            <div className="mt-3 text-center">
                {outputMessageProp.map((value, index) => {
                    return  <p key={index}>{value}</p>
                })}
               

                {/* Quy định việc ẩn hiện */}
                { likeImgProp ? <img src={likeImage} alt="Like" width={100}/> : null }

            </div>
        )
    }
}

export default LikeImage;