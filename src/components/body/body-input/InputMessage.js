import React, { Component } from "react";

class InputMessage extends Component {

    onBtnClickHandler=() =>{
        console.log("Nút đã được bấm");
        
        const {outputMessageChangeHandlerProp} = this.props;

        outputMessageChangeHandlerProp();

    }

    onInputChangeHandler = (event) =>{
        console.log("Input đã được nhập");

        console.log("Giá trị:", event.target.value);
        console.log(this.props);
        const  {inputMessageChangeHandlerProp} = this.props;
        inputMessageChangeHandlerProp(event.target.value);
    }

    render() {
        //console.log(this.props)
        const {inputMessageProp} = this.props;
        return(
            <React.Fragment>
                 <div className="row mt-3 form-group">
                    <label>Message cho bạn 12 tháng tới:</label>
                    <input placeholder="Nhập message của bạn" className="form-control" onChange={this.onInputChangeHandler} value={inputMessageProp}/>
                </div>
                <div className="row mt-3 justify-content-center">
                    <button className="btn btn-success" style={{width: "200px"}} onClick={this.onBtnClickHandler}>Gửi thông điệp</button>
                </div>
            </React.Fragment>
        )
    }
}

export default InputMessage;