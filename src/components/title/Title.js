import React, { Component } from "react";
import TitleImage from "./title-image/TitleImage";
import TitleText from "./title-text/TitleText";

class Title extends Component {
    render() {
        return(
            <React.Fragment>
                <TitleText />
                <TitleImage />
            </React.Fragment>
        )
    }
}

export default Title;